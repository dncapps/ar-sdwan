AR-sdwan
=========

Tasks related to Software Defined Networking. As is the case with AR-aws, this is monolithic in nature, and should be broken down into subroles based on the uses-cases.
(e.g. AR-switches, AR-asas...)

Requirements
------------

Network buy in.

Role Variables
--------------

Dependencies
------------

Example Playbook
----------------

License
-------

BSD

Author Information
------------------
