#!/usr/bin/env python

#
#nat [(real_ifc,mapped_ifc)] [line | {after-object [line]}] source static real_obj [mapped_obj | interface [ipv6]] [destination static {mapped_obj | interface [ipv6]} real_obj] [service real_src_mapped_dest_svc_obj mapped_src_real_dest_svc_obj] [net-to-net] [dns] [unidirectional | no-proxy-arp] [inactive] [description desc]
#
# nat [(real_ifc,mapped_ifc)] [line | after-auto [line]] source dynamic {real-obj | any} {mapped_obj [interface [ipv6]] | pat-pool mapped-obj [round-robin] [extended] [flat [include-reserve]] [block-allocation] [interface [ipv6]] | interface [ipv6]} [destination static {mapped_obj | interface [ipv6]} real_obj] [service mapped_dest_svc_obj real_dest_svc_obj] [unidirectional] [inactive] [description description]
#
#nat [(real_ifc,mapped_ifc)] dynamic {mapped_inline_host_ip | mapped_obj | pat-pool mapped-obj [round-robin] [extended] [flat [include-reserve]] [block-allocation] | interface [ipv6]} [interface [ipv6]]
#

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from ansible.errors import AnsibleError
from ansible.plugins.lookup import LookupBase
from ansible.utils.listify import


def nat(data):
    """ translates a data object into a nat rule """
    retval = [ "nat" ]
    if ('real_ifc' in data) and ('mapped_ifc' in data):
        retval += [ "(", str(data.real_ifc), ",", str(data.mapped_ifc), ")" ]
    if 'line' in data:
        retval += [ data.line ]
    if 'source' in data:
        if (data.source == 'static') and ('src_real_obj' in data) :
            retval += [ 'source','static',str(data.src_real_obj) ]
            if 'mapped_ob' in  data:
                retval += [ data.mapped_obj ]
            if 'destination' in data:
                retval += [ "destination", "static", str(data.mapped_obj)]
                retval += [ str(data.dst_real_obj) ]
            if 'service' in data:
                retval += [ "service", data.real_src_mapped_dest_svc_obj ]
                retval += [ data.mapped_src_dest_svc_obj ]
            if ('net_to_net' in data) and data.net_to_net:
                retval += ['net-to-net']
            if ('dns' in data) and data.dns:
                retval += ['dns']
            if ('unidirectional' in data) and data.unidirectional:
                retval += ['unidirectional']
            if ('no_proxy_arp' in data) and data.no_proxy_arp:
                retval += ['no-proxy-arp']
            if ('inactive' in data) and data.inactive:
                retval += ['inactive']
            if ('description' in data):
                retval += ['description',data.description]
            return ' '.join(retval)
        if ((data.source == 'dynamic') and ('src_real_obj' in data))
            retval += [ 'source','dynamic', str(data.src_real_obj) ]
            if 'pat_pool' in data:
                retval += [ 'pat-pool', data.pat_pool ]
                if 'round_robin' in data:
                    retval += [ 'round-robin']
                if 'extended' in data:
                    retval += [ 'extended ']
                if  'flat' in data:
                    retval += [ 'flat ']
                    if 'include_reserved' in data:
                        retval += ['include-reserve']
                if 'block_allocation' in data:
                    retval += [ 'block-allocation' ]
                if 'src_interface' in data:
                        retval += [ 'interface' ]
                        if 'src_ipv6' in data:
                            retval += [ 'ipv6' ]
            else if 'mapped_obj' in data:
                retval += [ str(data.mapped_obj) ]
                if 'src_interface' in data:
                    retval += [ 'src_interface' ]
                    if 'src_ipv6' in data:
                        retval += [ 'ipv6' ]
            else if 'src_interface' in data:
                retval += [ 'src_interface' ]
                if 'src_ipv6' in data:
                    retval += [ 'ipv6' ]
            if ('destination' in data) and (data.destination == 'static'):
                retval += [ 'destination','static' ]
                if 'dst_mapped_obj' in data;
                    retval += [ data.dst_mapped_obj ]
                else if 'dst_interface' in data:
                    retval += [ 'interface' ]
                    if 'dst_ipv6' in data:
                        retval += [ 'ipv6' ]
                    if 'dst_real_obj' in data:
                        retval += [ data.dst_real_obj  ]
            if ('service' in data) and ('mapped_dest_svc_obj' in data) and ('real_dest_svc_obj' in data):
                retval += [ ' service', data.mapped_dest_svc_obj, data.real_dest_svc_obj ]
            if 'unidirectional' in data:
                retval += [ 'unidirectional' ]
            if 'description' in data:
                retval += [ 'description', data.description ]
            return ' '.join(retval)
        if 'dynamic' in data:
            retval += ['dynamic']
            if 'mapped_inline_host_ip' in data:
                retval += [ data.mapped_inline_host_ip ]
           else  if 'mapped_obj' in data:
                retval += [ data.mapped_obj ]
           else if 'pat_pool' in data:
                retval += [ data.pat_pool ]
                if 'round_robin' in data:
                    retval += [ 'round-robin' ]
                if 'extended' in data:
                    retval += [ 'extended' ]
                if 'flat' in data:
                    retval += [ 'flat' ]
                    if 'include_reserve' in data:
                        retval += [ 'include-reserve' ]
                if 'block_allocation' in data:
                    retval += [ 'block-allocation']
                else if 'dst_interface' in data:
                    retval += [ 'interface' ]
                    if 'dst_ipv6' in data:
                        retval += [ 'ipv6' ]
           if 'dst_interface' in data:
               retval += [ 'interface' ]
               if 'dst_ipv6' in data:
                   retval += [ 'ipv6' ]
        ' '.join(retval)

#nat [(real_ifc,mapped_ifc)] dynamic {mapped_inline_host_ip | mapped_obj | pat-pool mapped-obj [round-robin] [extended] [flat [include-reserve]] [block-allocation] | interface [ipv6]} [interface [ipv6]]
#


class FilterModule(object):
    """ NAT rule filter """

    def filters(self):
        return {
            'nat' : nat
        }

if
